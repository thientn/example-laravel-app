**Example laravel app build docker**

Installation

Configuration
Clone this repository
Copy and make the required configuration changes in the .env file: cp .env.example .env
Install dependencies with Composer

For development: composer install

**Run docker :**

1- sudo docker-compose build 

2- sudo docker-compose up

For production, please see more at Laravel Deployment Guide
